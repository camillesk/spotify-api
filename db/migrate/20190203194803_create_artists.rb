class CreateArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :artists do |t|
      t.string :artist_id
      t.string :name
      t.string :spotify_url
      t.integer :followers
      t.string :genres, array: true
      t.integer :popularity

      t.timestamps
    end
  end
end
