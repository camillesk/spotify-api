namespace :populate_database do
  desc "Populate database from Spotify API"
  task following_api: :environment do
    api_url = "https://api.spotify.com/v1/me/following?type=artist"
    authorization_token = "BQCALXnJh_52Y3CpxlEvxV-BLZzVeNsl5Tang9smV-Y8dr_myijh_bQ09-2UeRt0XYKBeep6Jvmvwk_tGJxn4Nwj05iVa6eKG1HfHiUJ-6exD9Bctbwwtyjrrn3VqbN64RhfAeL4vtpcbhut13-IFhANGwiU"

    data = JSON.parse(RestClient.get "#{api_url}", {:Authorization => "Bearer #{authorization_token}"})

    data["artists"]["items"].each do |artist, index|
      existing_artist = Artist.find_by(artist_id: artist["id"])
      if !existing_artist
        new_artist = Artist.new(
          artist_id: artist["id"],
          name: artist["name"],
          spotify_url: artist["external_urls"]["spotify"],
          followers: artist["followers"]["total"],
          genres: artist["genres"],
          popularity: artist["popularity"],
        )
        if new_artist.save
          puts "saved artist"
        else
          puts "error saving artist"
        end
      else
        puts "artist already saved"
      end
    end
  end
end
