# README

**WORK IN PROGRESS**

This project gets the list of following artists from the spotify API.

Back-end: Ruby on Rails

Front-end: React

Steps:

* change the file `config/database.yml` to your database configs

* change the file `lib/tasks/populate_database.rake`, variable `authorization_token` to your authorization token from spotify to access the data.

*  run `bundle install`

*  run `rails db:create`

*  run `rails db:migrate`

*  run `rails populate_database:following_api`

In http://localhost:3000/api/v1/artists.json, it's possible to see the list.
