class Api::V1::ArtistsController < ApplicationController
  def index
    render json: Artist.all
  end

  def create
    artist = Artist.create(artist_params)
    render json: artist
  end

  def destroy
    Artist.destroy(params[:id])
  end

  def update
    artist = Artist.find(params[:id])
    artist.update_attributes(artist_params)
    render json: artist
  end

  private

  def artist_params
    params.require(:artist).permit(:id, :artist_id, :name, :spotify_url, :followers, :genres, :popularity)
  end
end
